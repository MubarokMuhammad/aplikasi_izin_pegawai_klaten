package id.go.kebumen.kapokmas.e_izinpegawai.Api;

public class UtilsAPI {

    public static final String BASE_ROOT_URL = "http://addkomputer.com/simpeg_klaten_api/api/";

    public static BaseApiService getApiService() {
        return RetrofitClient.getClient(BASE_ROOT_URL).create(BaseApiService.class);
    }

}
