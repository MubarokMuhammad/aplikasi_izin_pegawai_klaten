package id.go.kebumen.kapokmas.e_izinpegawai;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import id.go.kebumen.kapokmas.e_izinpegawai.Api.BaseApiService;
import id.go.kebumen.kapokmas.e_izinpegawai.Api.UtilsAPI;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfilActivity extends AppCompatActivity {

    private BaseApiService mApiService;
    EditText editText1,editText2;
    public String id_pegawai, id_status_pegawai, alamatx;

    TextView nama, alamat, no_hp, jabatan, eselon, instansi, nomor_induk;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        editText1 = (EditText) findViewById(R.id.no_hp);
        nomor_induk= (TextView) findViewById(R.id.nomor_induk);
        nama= (TextView) findViewById(R.id.nama);
        alamat= (TextView) findViewById(R.id.alamat);
        no_hp= (TextView) findViewById(R.id.no_hp_profil);
        jabatan= (TextView) findViewById(R.id.jabatan);
        eselon= (TextView) findViewById(R.id.eselon);
        instansi= (TextView) findViewById(R.id.instansi);

        Intent i = getIntent();
        id_pegawai=i.getStringExtra("id_pegawai");
        id_status_pegawai=i.getStringExtra("id_status_pegawai");

        mApiService = UtilsAPI.getApiService();
        Button button = findViewById(R.id.kirim);


        profil();

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ubahprofil();
            }
        });

    }

    private void profil(){
        String key="7d063bfb561bda7078d649dfeafe9aee";
        mApiService.datapokok(key, id_pegawai)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            /*loading.dismiss();*/
                            JSONObject jsonRESULTS = null;
                            try {
                                jsonRESULTS = new JSONObject(response.body().string());
                                nomor_induk.setText(jsonRESULTS.getString("nomor_induk"));
                                nama.setText(jsonRESULTS.getString("gelar_depan")+" "+jsonRESULTS.getString("nama")+" "+jsonRESULTS.getString("gelar_belakang"));
                                alamat.setText(jsonRESULTS.getString("alamat"));
                                no_hp.setText(jsonRESULTS.getString("no_hp"));
                                jabatan.setText(jsonRESULTS.getString("jabatan"));
                                eselon.setText(jsonRESULTS.getString("eselon")+"/"+jsonRESULTS.getString("golongan")+"/"+jsonRESULTS.getString("pangkat")+"/"+jsonRESULTS.getString("status_pegawai"));
                                instansi.setText(jsonRESULTS.getString("instansi"));

                                alamatx=jsonRESULTS.getString("alamat");

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        } else {
                            /*loading.dismiss();*/
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        /*Toast.makeText(getContext().getApplicationContext(), "Input yang anda masukan salah, silahkan coba kembali.", Toast.LENGTH_LONG).show();
                        loading.dismiss();*/
                    }
                });
    }



    private void ubahprofil(){
        String key="7d063bfb561bda7078d649dfeafe9aee";
        /*alamatx= "Condongcatur";*/
        mApiService.editDatapokok(id_pegawai, key, alamatx, "",editText1.getText().toString(), "")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            JSONObject jsonRESULTS = null;
                            try {
                                jsonRESULTS = new JSONObject(response.body().string());

                                Intent intent = new Intent(ProfilActivity.this, MainActivity.class);
                                Toast.makeText(ProfilActivity.this, "No. Telepon Berhasil diubah", Toast.LENGTH_LONG).show();
                                intent.putExtra("id_pegawai", id_pegawai);
                                intent.putExtra("id_status_pegawai", id_status_pegawai);
                                startActivity(intent);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        /*   Toast.makeText(LoginActivity.this, "Input yang anda masukan salah, silahkan coba kembali.", Toast.LENGTH_LONG).show();*/
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    this);
            alertDialogBuilder
                    .setTitle("Tentang")
                    .setMessage("sipp.klatenkab.go.id\nVersion 1.0")
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
            return true;
        }else if (id==android.R.id.home){
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
