package id.go.kebumen.kapokmas.e_izinpegawai.Model;

import java.util.List;

public class ResponseApel {
    private List<ListApel> listApels = null;

    public ResponseApel(List<ListApel> listApels) {
        this.listApels = listApels;
    }

    public List<ListApel> getListApels() {
        return listApels;
    }

    public void setListApels(List<ListApel> listApels) {
        this.listApels = listApels;
    }
}
