package id.go.kebumen.kapokmas.e_izinpegawai.Model;

import java.util.List;

public class ResponseGanti {
    private List<ListGanti> listGantis = null;

    public ResponseGanti(List<ListGanti> listGantis) {
        this.listGantis = listGantis;
    }

    public List<ListGanti> getListGantis() {
        return listGantis;
    }

    public void setListGantis(List<ListGanti> listGantis) {
        this.listGantis = listGantis;
    }
}
