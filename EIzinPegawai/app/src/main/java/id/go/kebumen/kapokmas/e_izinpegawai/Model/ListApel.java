package id.go.kebumen.kapokmas.e_izinpegawai.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListApel {

    @SerializedName("id_article")
    @Expose
    private String id_article;

    @SerializedName("article")
    @Expose
    private String article;

    @SerializedName("post_date")
    @Expose
    private String post_date;

    @SerializedName("post_time")
    @Expose
    private String post_time;

    @SerializedName("content")
    @Expose
    private String content;

    public ListApel(String id_article, String article, String post_date, String post_time, String content) {
        this.id_article = id_article;
        this.article = article;
        this.post_date = post_date;
        this.post_time = post_time;
        this.content = content;
    }


    public String getId_article() {
        return id_article;
    }

    public void setId_article(String id_article) {
        this.id_article = id_article;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public String getPost_time() {
        return post_time;
    }

    public void setPost_time(String post_time) {
        this.post_time = post_time;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
