package id.go.kebumen.kapokmas.e_izinpegawai.Fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import id.go.kebumen.kapokmas.e_izinpegawai.DaftarIzinActivity;
import id.go.kebumen.kapokmas.e_izinpegawai.GantiPassActivity;
import id.go.kebumen.kapokmas.e_izinpegawai.LoginActivity;
import id.go.kebumen.kapokmas.e_izinpegawai.MainActivity;
import id.go.kebumen.kapokmas.e_izinpegawai.ProfilActivity;
import id.go.kebumen.kapokmas.e_izinpegawai.R;

public class Tentang extends Fragment {
    LinearLayout keluar, ganti, profil;
    String id_pegawai, id_status_pegawai;
    public Tentang() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_tentang, container, false);

        final MainActivity activity = (MainActivity) getActivity();
        id_pegawai = activity.getId_pegawai();
        id_status_pegawai = activity.getId_status_pegawai();

        ganti=(LinearLayout) v.findViewById(R.id.ganti);
        ganti.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getContext().getApplicationContext(), GantiPassActivity.class);
                intent.putExtra("id_pegawai", id_pegawai);
                intent.putExtra("id_status_pegawai", id_status_pegawai);
                startActivity(intent);
            }
        });

        profil=(LinearLayout) v.findViewById(R.id.profil);
        profil.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getContext().getApplicationContext(), ProfilActivity.class);
                intent.putExtra("id_pegawai", id_pegawai);
                intent.putExtra("id_status_pegawai", id_status_pegawai);
                startActivity(intent);
            }
        });

        keluar=(LinearLayout) v.findViewById(R.id.keluar);
        keluar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getContext().getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            }
        });

        return v;
    }
}
