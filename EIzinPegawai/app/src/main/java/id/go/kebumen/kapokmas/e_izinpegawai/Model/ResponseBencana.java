package id.go.kebumen.kapokmas.e_izinpegawai.Model;

import java.util.List;

public class ResponseBencana {

    private List<ListBencana> listBencanas = null;

    public ResponseBencana(List<ListBencana> listBencanas) {
        this.listBencanas = listBencanas;
    }

    public List<ListBencana> getListBencanas() {
        return listBencanas;
    }

    public void setListBencanas(List<ListBencana> listBencanas) {
        this.listBencanas = listBencanas;
    }
}
