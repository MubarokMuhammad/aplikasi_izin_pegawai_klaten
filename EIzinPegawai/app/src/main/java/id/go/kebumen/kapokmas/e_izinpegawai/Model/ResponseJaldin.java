package id.go.kebumen.kapokmas.e_izinpegawai.Model;

import java.util.List;

public class ResponseJaldin {
    private List<ListJaldin> listJaldins = null;

    public ResponseJaldin(List<ListJaldin> listJaldins) {
        this.listJaldins = listJaldins;
    }

    public List<ListJaldin> getListJaldins() {
        return listJaldins;
    }

    public void setListJaldins(List<ListJaldin> listJaldins) {
        this.listJaldins = listJaldins;
    }
}
