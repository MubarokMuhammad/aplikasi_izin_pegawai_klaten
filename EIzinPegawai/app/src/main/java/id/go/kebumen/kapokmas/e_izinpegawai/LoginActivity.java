package id.go.kebumen.kapokmas.e_izinpegawai;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import id.go.kebumen.kapokmas.e_izinpegawai.Api.BaseApiService;
import id.go.kebumen.kapokmas.e_izinpegawai.Api.UtilsAPI;
import id.go.kebumen.kapokmas.e_izinpegawai.Helper.JSONParser;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {


    Button button;
    TextView lupa, register;
    EditText editText1,editText2;
    CheckBox checkbox1;
    JSONArray rooms = null;
    ArrayList<HashMap<String, String>> arraylist;
    ProgressDialog mProgressDialog;
    JSONParser jsonParser = new JSONParser();
    ArrayList<HashMap<String, String>> requestList;
    // tracks JSONArray
    JSONArray requests = null;
    private String json, status, id, nama, alamat;
    private static String TAG_STATUS = "status";
    private static String TAG_DATA = "data";
    private BaseApiService mApiService;
    ProgressDialog loading;

    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);



        editText1 = (EditText) findViewById(R.id.email);
        editText2 = (EditText) findViewById(R.id.password);

        mApiService = UtilsAPI.getApiService();

        checkbox1 = (CheckBox) findViewById(R.id.checkBox); //Memanggil ID Checkbox

        /*Membuat Aksi Ketika Checkbox Di Klik*/
        checkbox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked){
                    //Jika checkbox di ceklis maka tampilkan password
                    editText2.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }else{
                    //Jika checkbox tidak di ceklis maka sembunyikan password
                    editText2.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });
        button = (Button) findViewById(R.id.btnLogin);

        lupa=(TextView) findViewById(R.id.btnLupaPassword);

        lupa.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, LupaPassActivity.class));

            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                loading = ProgressDialog.show(LoginActivity.this, null, "Harap Tunggu...", true, false);
                requestLogin();
            }
        });

    }

    private void requestLogin(){
        String key="7d063bfb561bda7078d649dfeafe9aee";
        mApiService.loginRequest(key, editText1.getText().toString(), editText2.getText().toString())
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            loading.dismiss();
                            JSONObject jsonRESULTS = null;
                        try {
                            jsonRESULTS = new JSONObject(response.body().string());
                            if (jsonRESULTS.getString("status").equals("false")) {
                                Toast.makeText(LoginActivity.this, "Input yang anda masukan salah, silahkan coba kembali.", Toast.LENGTH_LONG).show();
                            }else {
                                String id_pegawai = jsonRESULTS.getString("id_pegawai");
                                String id_status_pegawai = jsonRESULTS.getString("id_status_pegawai");

                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                intent.putExtra("id_pegawai", id_pegawai);
                                intent.putExtra("id_status_pegawai", id_status_pegawai);
                                startActivity(intent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        } else {
                            loading.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(LoginActivity.this, "Input yang anda masukan salah, silahkan coba kembali.", Toast.LENGTH_LONG).show();
                        loading.dismiss();
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    this);
            alertDialogBuilder
                    .setTitle("Tentang")
                    .setMessage("sipp.klatenkab.go.id\nVersion 1.0")
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}