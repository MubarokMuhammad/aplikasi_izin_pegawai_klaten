package id.go.kebumen.kapokmas.e_izinpegawai.Api;

import java.util.List;

import id.go.kebumen.kapokmas.e_izinpegawai.Model.ListApel;
import id.go.kebumen.kapokmas.e_izinpegawai.Model.ListBelajar;
import id.go.kebumen.kapokmas.e_izinpegawai.Model.ListBencana;
import id.go.kebumen.kapokmas.e_izinpegawai.Model.ListCuti;
import id.go.kebumen.kapokmas.e_izinpegawai.Model.ListGanti;
import id.go.kebumen.kapokmas.e_izinpegawai.Model.ListJaldin;
import id.go.kebumen.kapokmas.e_izinpegawai.Model.ListSakit;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface BaseApiService {

    @GET("{tipe}")
    Call<List<ListJaldin>> getJaldin(@Path("tipe") String tipe, @Query("key") String key, @Query("id_pegawai") String id_pegawai, @Query("tahun") String tahun);

    @FormUrlEncoded
    @POST("auth")
    Call<ResponseBody> loginRequest(@Field("key") String key, @Field("nomor_induk") String nomor_induk, @Field("passwd") String passwd);

    @GET("ringkasan")
    Call<ResponseBody> ringkasan(@Query("key") String key, @Query("id_pegawai") String id_pegawai);

    @GET("pemberitahuan")
    Call<List<ListApel>> pemberitahuan(@Query("key") String key, @Query("id_pegawai") String id_pegawai);

    @FormUrlEncoded
    @POST("izin")
    Call<ResponseBody> tambahIzin(@Field("key") String key,
                                  @Field("id_pegawai") String id_pegawai, @Field("tgl_mulai") String tgl_mulai, @Field("tgl_akhir") String tgl_akhir,
                                  @Field("id_tipe") String id_tipe, @Field("keterangan") String keterangan, @Field("file_bukti") String file_bukti);

    @GET("data_pokok")
    Call<ResponseBody> datapokok(@Query("key") String key, @Query("id_pegawai") String id_pegawai);

    @FormUrlEncoded
    @POST("data_pokok/{id_pegawai}")
    Call<ResponseBody> editDatapokok(@Path("id_pegawai") String id_pegawai, @Field("key") String key, @Field("alamat") String alamat,
                                     @Field("kode_pos_rumah") String kode_pos_rumah, @Field("no_hp") String no_hp, @Field("npwp") String npwp);

    @FormUrlEncoded
    @POST("change_pass/{id_pegawai}")
    Call<ResponseBody> editPasswd(@Path("id_pegawai") String id_pegawai, @Field("key") String key, @Field("passwd") String passwd, @Field("passwd_ulang") String passwd_ulang);
}
