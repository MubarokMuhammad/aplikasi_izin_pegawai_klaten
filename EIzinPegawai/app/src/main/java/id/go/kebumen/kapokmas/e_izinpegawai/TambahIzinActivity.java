package id.go.kebumen.kapokmas.e_izinpegawai;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ipaulpro.afilechooser.utils.FileUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import id.go.kebumen.kapokmas.e_izinpegawai.Api.BaseApiService;
import id.go.kebumen.kapokmas.e_izinpegawai.Api.UtilsAPI;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahIzinActivity extends AppCompatActivity {

    EditText txt_tgl_awal, txt_tgl_akhir, keterangan;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date, dates;
    TextView textViewPathA;
    Button uploadA;
    private int REQUEST_CHOOSER = 1, Fak = 2, Cuk=3, Dea=4, Eak=5;
    int serverResponseCode = 0;
    ProgressDialog dialog = null;
    Spinner kategori;
    public String file_name, tglmulai, tglakhir, tglmulaisms, tglakhirsms, no, msg, tujuan;
    private BaseApiService mApiService;

    String id_pegawai, id_status_pegawai, id_tipe;
    private String[] kategori_data = {
            "Dinas Dalam Daerah",
            "Dinas Luar Daerah",
            "Tugas Belajar",
            "Sakit",
            "Cuti Tahunan",
            "Cuti Besar",
            "Cuti Alasan Penting",
            "Cuti Diluar Tanggungan Negara",
            "Cuti Bersalin",
            "Bencana",
            "Ganti Piket/ Ganti Cuti",
            "Apel Bersama"
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_izin);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent i = getIntent();
        id_pegawai=i.getStringExtra("id_pegawai");
        id_status_pegawai=i.getStringExtra("id_status_pegawai");

        mApiService = UtilsAPI.getApiService();

        txt_tgl_awal = (EditText) findViewById(R.id.tgl_mulai);
        txt_tgl_akhir = (EditText) findViewById(R.id.tgl_selesai);
        keterangan = (EditText) findViewById(R.id.keterangan);


        myCalendar = Calendar.getInstance();

        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

        dates = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabels();
            }
        };

        txt_tgl_awal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(TambahIzinActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        txt_tgl_akhir.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(TambahIzinActivity.this, dates, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        Button lanjut = (Button) findViewById(R.id.kirim);


        textViewPathA = (TextView) findViewById(R.id.textViewPathA);
        textViewPathA.setText("");

        if(!isStoragePermissionGranted())
        {

        }


        uploadA = (Button) findViewById(R.id.uploadA);
        uploadA.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent getContentIntent = FileUtils.createGetContentIntent();
                Intent intent = Intent
                        .createChooser(getContentIntent, "Pilih file");
                startActivityForResult(intent, REQUEST_CHOOSER);
            }
        });


        kategori = (Spinner) findViewById(R.id.kategori);

        final ArrayAdapter<String> kl = new ArrayAdapter<String>(this, R. layout. support_simple_spinner_dropdown_item, kategori_data);
        kategori.setAdapter(kl);

        kategori.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // memunculkan toast + value Spinner yang dipilih (diambil dari adapter)
                /*String dd, dl, tb, sakit, ct, cb, cap, cdtn, cutib, bencana, gp, ab;*/
                String z=kl.getItem(i);
                if(z=="Dinas Dalam Daerah"){
                    tujuan=z;
                    id_tipe="21";
                }else if(z=="Dinas Luar Daerah"){
                    tujuan="Dinas Luar Daerah";
                    id_tipe="22";
                }else if(z=="Tugas Belajar"){
                    tujuan=z;
                    id_tipe="3";
                }else if(z=="Sakit"){
                    tujuan=z;
                    id_tipe="4";
                }else if(z=="Cuti Tahunan"){
                    tujuan=z;
                    id_tipe="51";
                }else if(z=="Cuti Besar"){
                    tujuan=z;
                    id_tipe="52";
                }else if(z=="Cuti Alasan Penting"){
                    tujuan=z;
                    id_tipe="53";
                }else if(z=="Cuti Diluar Tanggungan Negara"){
                    tujuan=z;
                    id_tipe="54";
                }else if(z=="Cuti Bersalin"){
                    id_tipe="55";
                    tujuan=z;
                }else if(z=="Bencana"){
                    id_tipe="6";
                    tujuan=z;
                }else if(z=="Ganti Piket/ Ganti Cuti"){
                    id_tipe="7";
                    tujuan=z;
                }else if(z=="Apel Bersama"){
                    id_tipe="8";
                    tujuan=z;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });





        lanjut.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int idxx= Integer.parseInt(id_status_pegawai);

                if(idxx==2){

                    no="085802751751";

                    if(id_tipe=="21"){
                        msg="SAE#DD#"+keterangan.getText().toString()+"#"+tglmulaisms+"#"+tglakhirsms+"#";
                    }else if(tujuan=="Dinas Luar Daerah"){
                        msg="SAE#DL#"+keterangan.getText().toString()+"#"+tglmulaisms+"#"+tglakhirsms+"#";
                    }else if(id_tipe=="3"){
                        msg="SAE#TB#"+keterangan.getText().toString()+"#"+tglmulaisms+"#"+tglakhirsms+"#";
                    }else if(id_tipe=="4"){
                        msg="SAE#SAKIT#"+keterangan.getText().toString()+"#"+tglmulaisms+"#"+tglakhirsms+"#";
                    }else if(id_tipe=="51"){
                        msg="SAE#TAHUNAN#"+keterangan.getText().toString()+"#"+tglmulaisms+"#"+tglakhirsms+"#";
                    }else if(id_tipe=="52"){
                        msg="SAE#BESAR#"+keterangan.getText().toString()+"#"+tglmulaisms+"#"+tglakhirsms+"#";
                    }else if(id_tipe=="53"){
                        msg="SAE#PENTING#"+keterangan.getText().toString()+"#"+tglmulaisms+"#"+tglakhirsms+"#";
                    }else if(id_tipe=="54"){
                        msg="SAE#CLTNI#"+keterangan.getText().toString()+"#"+tglmulaisms+"#"+tglakhirsms+"#";
                    }else if(id_tipe=="55"){
                        msg="SAE#MELAHIRKAN#"+keterangan.getText().toString()+"#"+tglmulaisms+"#"+tglakhirsms+"#";
                    }else if(id_tipe=="6"){
                        msg="SAE#BENCANA#"+keterangan.getText().toString()+"#"+tglmulaisms+"#"+tglakhirsms+"#";
                    }else if(id_tipe=="7"){
                        msg="SAE#GANTI#"+keterangan.getText().toString()+"#"+tglmulaisms+"#"+tglakhirsms+"#";
                    }else if(id_tipe=="8"){
                        msg="SAE#APELBERSAMA#"+keterangan.getText().toString()+"#"+tglmulaisms+"#"+tglakhirsms+"#";
                    }else {
                        msg="Kosong\n(Silahkan Pilih Tujuan Izin Terlebih Dahulu)";
                    }

                    CekAlert();

                }else{
                    CekAlertKedua();
                }

            }
        });

    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)+
                    checkSelfPermission(Manifest.permission.READ_SMS)+
                    checkSelfPermission(Manifest.permission.SEND_SMS)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {

                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_SMS,
                        Manifest.permission.SEND_SMS}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == REQUEST_CHOOSER) {

            final Uri uri = data.getData();
            final String path = FileUtils.getPath(this, uri);
            textViewPathA.setText(path);
            /*Toast.makeText(TambahIzinActivity.this, "Berhasil.",
                    Toast.LENGTH_SHORT).show();*/

        }
    }

    public void kirimIzinSms(String phoneNo, String msg) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
            /*Toast.makeText(getApplicationContext(), "Message Sent",
                    Toast.LENGTH_LONG).show();*/

        } catch (Exception ex) {
            gagalAlert();
            ex.printStackTrace();
        }
    }

    public void CekAlert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Detail Permohonan Anda")
                .setMessage(
                        "\nTujuan Izin:\n\t\t\t\t\t\t\t"+tujuan+
                        "\nTanggal Mulai:\n\t\t\t\t\t\t\t"+tglmulaisms+
                                "\nTanggal Akhir:\n\t\t\t\t\t\t\t"+tglakhirsms+
                                "\nKeterangan:\n\t\t\t\t\t\t\t"+keterangan.getText().toString()+
                        "\nNo. Tujuan:\n\t\t\t\t\t\t\t"+no+
                                "\nSMS:\n"+msg)
                .setNegativeButton("Cek Kembali", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton("Kirimkan", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
//                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
//                        intent.putExtra("id_pegawai", id_pegawai);
//                        intent.putExtra("id_status_pegawai", id_status_pegawai);
//                        startActivity(intent);

                        kirimIzinSms(no, msg);

                        final String pathE = textViewPathA.getText().toString();
                        if (pathE != null && FileUtils.isLocal(pathE)) {
                            File file = new File(pathE);
                        }

                        dialog = ProgressDialog.show(TambahIzinActivity.this, "",
                                "Upload file...", true);

                        new Thread(new Runnable() {
                            public void run() {
                                runOnUiThread(new Runnable() {
                                    public void run() {

                                    }
                                });

                                int response = 0;

                                String judulE="142";
                                response = uploadFileA(pathE, judulE);
                                tambahizin();
                                /*suksesAlert();*/

                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                intent.putExtra("id_pegawai", id_pegawai);
                                intent.putExtra("id_status_pegawai", id_status_pegawai);
                                startActivity(intent);
                            }
                        }).start();

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }

    public void CekAlertKedua(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Detail Permohonan Anda")
                .setMessage(
                        "\nTujuan Izin:\n\t\t\t\t\t\t\t"+tujuan+
                                "\nTanggal Mulai:\n\t\t\t\t\t\t\t"+tglmulaisms+
                                "\nTanggal Akhir:\n\t\t\t\t\t\t\t"+tglakhirsms+
                                "\nKeterangan:\n\t\t\t\t\t\t\t"+keterangan.getText().toString())
                .setNegativeButton("Cek Kembali", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton("Kirimkan", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        final String pathE = textViewPathA.getText().toString();
                        if (pathE != null && FileUtils.isLocal(pathE)) {
                            File file = new File(pathE);
                        }

                        dialog = ProgressDialog.show(TambahIzinActivity.this, "",
                                "Upload file...", true);

                        new Thread(new Runnable() {
                            public void run() {
                                runOnUiThread(new Runnable() {
                                    public void run() {

                                    }
                                });

                                int response = 0;

                                String judulE="142";
                                response = uploadFileA(pathE, judulE);
                                tambahizin();
                                /*suksesAlert();*/

                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                intent.putExtra("id_pegawai", id_pegawai);
                                intent.putExtra("id_status_pegawai", id_status_pegawai);
                                startActivity(intent);
                            }
                        }).start();

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }

    public void gagalAlert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Permohonan Izin Gagal")
                .setMessage("Silahkan cek jaringan dan pulsa anda, untuk dapat mengajukan izin lewat aplikasi.")
                .setPositiveButton("Coba Kembali", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setNegativeButton("Tutup", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.putExtra("id_pegawai", id_pegawai);
                        intent.putExtra("id_status_pegawai", id_status_pegawai);
                        startActivity(intent);
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public int uploadFileA(String sourceFileUri, String judul) {

        // ip komputer server
        String upLoadServerUri = "http://addkomputer.com/simpeg_klaten_api/api/upload";
        String fileName = sourceFileUri;

        String key="7d063bfb561bda7078d649dfeafe9aee";

        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(sourceFileUri);
        if (!sourceFile.isFile()) {
            Log.e("uploadFile", "Source File Does not exist");
            return 0;
        }
        try {
            FileInputStream fileInputStream = new FileInputStream(sourceFile);
            URL url = new URL(upLoadServerUri);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true); // Allow Inputs
            conn.setDoOutput(true); // Allow Outputs
            conn.setUseCaches(false); // Don't use a Cached Copy
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Content-Type",
                    "multipart/form-data;boundary=" + boundary);
            conn.setRequestProperty("file_upload", fileName);
            dos = new DataOutputStream(conn.getOutputStream());
            dos.writeBytes(twoHyphens + boundary + lineEnd);

            // untuk parameter keterangan
            dos.writeBytes("Content-Disposition: form-data; name=\"key\""
                    + lineEnd);
            dos.writeBytes(lineEnd);
            dos.writeBytes(key);
            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + lineEnd);

            dos.writeBytes("Content-Disposition: form-data; name=\"file_upload\";filename=\""
                    + fileName + "\"" + lineEnd);
            dos.writeBytes(lineEnd);
            // create a buffer of maximum size
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];
            // read file and write it into form...
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            while (bytesRead > 0) {
                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            serverResponseCode = conn.getResponseCode();
            String serverResponseMessage = conn.getResponseMessage();
            final InputStream in = conn.getInputStream();

            byte data[] = new byte[1024];
            int counter = -1;
            String jsonString = "";
            while(true){
                try {
                    if (!((counter = in.read(data)) != -1)) break;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                jsonString += new String(data, 0, counter);
            }

            JSONObject jObj = new JSONObject(jsonString);
            file_name=jObj.getString("file_name");

            if (serverResponseCode == 200) {
                runOnUiThread(new Runnable() {
                    public void run() {

                    }
                });
            }

            // close the streams //
            fileInputStream.close();
            dos.flush();
            dos.close();

        } catch (MalformedURLException ex) {
            ex.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();

        }
        return serverResponseCode;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){


            // permission granted

        }
    }

    private void tambahizin(){
        String key="7d063bfb561bda7078d649dfeafe9aee";
        mApiService.tambahIzin(key, id_pegawai, tglmulai, tglakhir, id_tipe, keterangan.getText().toString(), file_name)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            JSONObject jsonRESULTS = null;
                            try {
                                jsonRESULTS = new JSONObject(response.body().string());
                   /*                  String id_pegawai = jsonRESULTS.getString("id_pegawai");
                                    String id_status_pegawai = jsonRESULTS.getString("id_status_pegawai");

                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                    intent.putExtra("id_pegawai", id_pegawai);
                                    intent.putExtra("id_status_pegawai", id_status_pegawai);
                                    startActivity(intent);*/

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
    }

    private void updateLabel() {
        String myFormat = "dd-MM-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        txt_tgl_awal.setText(sdf.format(myCalendar.getTime()));
        String myFormatzz = "yyyy-MM-dd";
        SimpleDateFormat sdfzz = new SimpleDateFormat(myFormatzz, Locale.US);
        tglmulai=sdfzz.format(myCalendar.getTime());
        String myFormatqq = "dd/MM/yyyy";
        SimpleDateFormat sdfqq = new SimpleDateFormat(myFormatqq, Locale.US);
        tglmulaisms=sdfqq.format(myCalendar.getTime());
    }

    private void updateLabels() {
        String myFormat = "dd-MM-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        txt_tgl_akhir.setText(sdf.format(myCalendar.getTime()));
        String myFormatxx = "yyyy-MM-dd";
        SimpleDateFormat sdfxx = new SimpleDateFormat(myFormatxx, Locale.US);
        tglakhir=sdfxx.format(myCalendar.getTime());
        String myFormatqq = "dd/MM/yyyy";
        SimpleDateFormat sdfqq = new SimpleDateFormat(myFormatqq, Locale.US);
        tglakhirsms=sdfqq.format(myCalendar.getTime());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    this);
            alertDialogBuilder
                    .setTitle("Tentang")
                    .setMessage("sipp.klatenkab.go.id\nVersion 1.0")
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
            return true;
        }else if (id==android.R.id.home){
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
