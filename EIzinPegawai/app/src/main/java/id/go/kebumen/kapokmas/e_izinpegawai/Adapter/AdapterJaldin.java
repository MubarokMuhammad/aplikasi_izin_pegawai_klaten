package id.go.kebumen.kapokmas.e_izinpegawai.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import id.go.kebumen.kapokmas.e_izinpegawai.Model.ListGanti;
import id.go.kebumen.kapokmas.e_izinpegawai.Model.ListJaldin;
import id.go.kebumen.kapokmas.e_izinpegawai.R;

public class AdapterJaldin extends RecyclerView.Adapter<AdapterJaldin.MyViewHolder> {

    private List<ListJaldin> moviesList;
    private Activity activity;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView id, tipe, tanggal, keterangan;
        public LinearLayout ll_layout_utama;

        public MyViewHolder(View view) {
            super(view);
            id = view.findViewById(R.id.id);
            tipe = view.findViewById(R.id.tipe);
            tanggal = view.findViewById(R.id.tanggal);
            keterangan=view.findViewById(R.id.keterangan);
            ll_layout_utama = view.findViewById(R.id.ll_layout_utama);
        }
    }

    public AdapterJaldin(List<ListJaldin> moviesList, Activity activity) {
        this.moviesList = moviesList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listview_izin, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ListJaldin movie = moviesList.get(position);


        holder.id.setText(movie.getId());
        holder.tipe.setText(movie.getTipe());

        Locale idl = new Locale("in", "ID");
        String pattern = "EEEE, dd MMM yyyy";

        SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat myFormat = new SimpleDateFormat(pattern, idl);

        try {

            String reformattedStr = myFormat.format(fromUser.parse(movie.getTanggal()));
            holder.tanggal.setText(reformattedStr+" \t");
        } catch (ParseException e) {
            e.printStackTrace();
        }


        holder.keterangan.setText(movie.getKeterangan());


        holder.ll_layout_utama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

        /*        Intent goDetail = new Intent(activity, DetailBeritaActivity.class);
                goDetail.putExtra("post_title", movie.getPost_title());
                goDetail.putExtra("post_content", movie.getPost_content());
                goDetail.putExtra("post_date", movie.getPost_date());
                goDetail.putExtra("image_name", movie.getImage_name());

                activity.startActivity(goDetail);*/
            }
        });

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

}
