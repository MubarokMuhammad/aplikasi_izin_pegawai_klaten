package id.go.kebumen.kapokmas.e_izinpegawai.Model;

import java.util.List;

public class ResponseSakit {
    private List<ListSakit> listSakits = null;

    public ResponseSakit(List<ListSakit> listSakits) {
        this.listSakits = listSakits;
    }

    public List<ListSakit> getListSakits() {
        return listSakits;
    }

    public void setListSakits(List<ListSakit> listSakits) {
        this.listSakits = listSakits;
    }
}
