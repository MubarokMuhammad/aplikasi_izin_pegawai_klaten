package id.go.kebumen.kapokmas.e_izinpegawai.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.go.kebumen.kapokmas.e_izinpegawai.Adapter.AdapterJaldin;
import id.go.kebumen.kapokmas.e_izinpegawai.Adapter.NotifikasiAdapter;
import id.go.kebumen.kapokmas.e_izinpegawai.Api.BaseApiService;
import id.go.kebumen.kapokmas.e_izinpegawai.Api.UtilsAPI;
import id.go.kebumen.kapokmas.e_izinpegawai.DaftarIzinActivity;
import id.go.kebumen.kapokmas.e_izinpegawai.MainActivity;
import id.go.kebumen.kapokmas.e_izinpegawai.Model.ListApel;
import id.go.kebumen.kapokmas.e_izinpegawai.Model.ListJaldin;
import id.go.kebumen.kapokmas.e_izinpegawai.Model.ResponseApel;
import id.go.kebumen.kapokmas.e_izinpegawai.Model.ResponseJaldin;
import id.go.kebumen.kapokmas.e_izinpegawai.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Pemberitahuan extends Fragment {
    LinearLayout keluar;
    private RecyclerView mRecycler;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mManager;
    private List<ListApel> mItems = new ArrayList<>();
    ProgressDialog pd;

    private List<ResponseApel> mCategoryDataList = new ArrayList<>();
    private List<ListApel> listCategory = new ArrayList<>();
    private RecyclerView rc_list_rating;

    private BaseApiService mApiService;
    private NotifikasiAdapter CategoryAdapter;

    private ProgressBar pb_load;
    private ShimmerRecyclerView shimmer_recycler_view;

    String id, key, id_pegawai, tipe, tahun, id_status_pegawai;

    public Pemberitahuan() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_pemberitahuan, container, false);

        final MainActivity activity = (MainActivity) getActivity();
        id_pegawai = activity.getId_pegawai();

        rc_list_rating = (RecyclerView) v.findViewById(R.id.rc_list_category);
        pb_load = (ProgressBar) v.findViewById(R.id.pb_load);
        shimmer_recycler_view = (ShimmerRecyclerView) v.findViewById(R.id.shimmer_recycler_view);
        mApiService = UtilsAPI.getApiService();

        LinearLayoutManager layoutManagerCategory
                = new LinearLayoutManager(getContext().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);

        // ini vertical
        RecyclerView.LayoutManager mLayoutManager =
                new LinearLayoutManager(getContext().getApplicationContext());

        CategoryAdapter = new NotifikasiAdapter(listCategory, getActivity());
        rc_list_rating.setLayoutManager(mLayoutManager);
        rc_list_rating.setItemAnimator(new DefaultItemAnimator());
        rc_list_rating.setAdapter(CategoryAdapter);

        return v;
    }

    private void dataAttachmentCategory() {
        rc_list_rating.setVisibility(View.GONE);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        tahun = sdf.format(new Date());

        key="7d063bfb561bda7078d649dfeafe9aee";
        mCategoryDataList.clear();
        listCategory.clear();
        mApiService.pemberitahuan(
                key,
                id_pegawai)
                .enqueue(new Callback<List<ListApel>>() {
                    @Override
                    public void onResponse(Call<List<ListApel>> call, Response<List<ListApel>> response) {

                        pb_load.setVisibility(View.GONE);
                        shimmer_recycler_view.hideShimmerAdapter();
                        List<ListApel> listCategories = response.body();
                        if (listCategories != null&& listCategories.size()>0) {
                            for (int i = 0; i < listCategories.size(); i++) {
                                ListApel modelSeatGroup = new ListApel(
                                        response.body().get(i).getId_article(),
                                        response.body().get(i).getArticle(),
                                        response.body().get(i).getPost_date(),
                                        response.body().get(i).getPost_time(),
                                        response.body().get(i).getContent());
                                listCategory.add(modelSeatGroup);
                            }

                            CategoryAdapter = new NotifikasiAdapter(listCategory, getActivity());
                            rc_list_rating.setAdapter(CategoryAdapter);

                            if (listCategory.isEmpty()) {
                                rc_list_rating.setVisibility(View.GONE);

                                Toast.makeText(getContext().getApplicationContext(), "Data tidak ditemukan", Toast.LENGTH_SHORT).show();
                            } else {
                                rc_list_rating.setVisibility(View.VISIBLE);
                            }
                            //get values
                        }else{
                            //show alert not get value
                        }

                    }


                    @Override
                    public void onFailure(Call<List<ListApel>> call, Throwable t) {
                        pb_load.setVisibility(View.GONE);
                        shimmer_recycler_view.hideShimmerAdapter();
                        Toast.makeText(getContext().getApplicationContext(), "Silahkan cek kembali koneksi anda.", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();

        pb_load.setVisibility(View.GONE);
        shimmer_recycler_view.showShimmerAdapter();
        dataAttachmentCategory();
    }

}