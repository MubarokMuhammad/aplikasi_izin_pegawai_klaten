package id.go.kebumen.kapokmas.e_izinpegawai.Model;

import java.util.List;

public class ResponseBelajar {
    private List<ListBelajar> listBelajars = null;

    public ResponseBelajar(List<ListBelajar> listBelajars) {
        this.listBelajars = listBelajars;
    }

    public List<ListBelajar> getListBelajars() {
        return listBelajars;
    }

    public void setListBelajars(List<ListBelajar> listBelajars) {
        this.listBelajars = listBelajars;
    }
}
