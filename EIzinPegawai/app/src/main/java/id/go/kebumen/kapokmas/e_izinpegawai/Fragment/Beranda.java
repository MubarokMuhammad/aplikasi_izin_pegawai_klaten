package id.go.kebumen.kapokmas.e_izinpegawai.Fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import id.go.kebumen.kapokmas.e_izinpegawai.Api.BaseApiService;
import id.go.kebumen.kapokmas.e_izinpegawai.Api.UtilsAPI;
import id.go.kebumen.kapokmas.e_izinpegawai.DaftarIzinActivity;
import id.go.kebumen.kapokmas.e_izinpegawai.LoginActivity;
import id.go.kebumen.kapokmas.e_izinpegawai.MainActivity;
import id.go.kebumen.kapokmas.e_izinpegawai.R;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Beranda extends Fragment {
    LinearLayout jaldin, tugas_belajar, sakit, cuti, bencana, ganti, apel;
    TextView jumlah_minggu,
            jumlah_bulan,
            jumlah_tahun,
            jumlah_sisa_cuti,
            jumlah_jaldin,
            jumlah_tugas_belajar,
            jumlah_sakit,
            jumlah_cuti,
            jumlah_bencana,
            jumlah_ganti,
            jumlah_apel;

    public String tipe, id_pegawai, id_status_pegawai;

    private BaseApiService mApiService;

    public Beranda() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_beranda, container, false);

        final MainActivity activity = (MainActivity) getActivity();
        id_pegawai = activity.getId_pegawai();
        id_status_pegawai = activity.getId_status_pegawai();

        jumlah_minggu=v.findViewById(R.id.jumlah_minggu);
        jumlah_bulan=v.findViewById(R.id.jumlah_bulan);
        jumlah_tahun=v.findViewById(R.id.jumlah_tahun);
        jumlah_sisa_cuti=v.findViewById(R.id.jumlah_sisa_cuti);
        jumlah_jaldin=v.findViewById(R.id.jumlah_jaldin);
        jumlah_tugas_belajar=v.findViewById(R.id.jumlah_tugas_belajar);
        jumlah_sakit=v.findViewById(R.id.jumlah_sakit);
        jumlah_cuti=v.findViewById(R.id.jumlah_cuti);
        jumlah_bencana=v.findViewById(R.id.jumlah_bencana);
        jumlah_ganti=v.findViewById(R.id.jumlah_ganti);
        jumlah_apel=v.findViewById(R.id.jumlah_apel);


        jaldin=(LinearLayout) v.findViewById(R.id.jaldin);
        jaldin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getContext().getApplicationContext(), DaftarIzinActivity.class);
                tipe="jaldin";
                intent.putExtra("tipe", tipe);
                intent.putExtra("id_pegawai", id_pegawai);
                intent.putExtra("id_status_pegawai", id_status_pegawai);
                startActivity(intent);
            }
        });

        tugas_belajar=(LinearLayout) v.findViewById(R.id.tugas_belajar);
        tugas_belajar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getContext().getApplicationContext(), DaftarIzinActivity.class);
                tipe="tugas_belajar";
                intent.putExtra("tipe", tipe);
                intent.putExtra("id_pegawai", id_pegawai);
                intent.putExtra("id_status_pegawai", id_status_pegawai);
                startActivity(intent);
            }
        });

        sakit=(LinearLayout) v.findViewById(R.id.sakit);
        sakit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getContext().getApplicationContext(), DaftarIzinActivity.class);
                tipe="sakit";
                intent.putExtra("tipe", tipe);
                intent.putExtra("id_pegawai", id_pegawai);
                intent.putExtra("id_status_pegawai", id_status_pegawai);
                startActivity(intent);
            }
        });

        cuti=(LinearLayout) v.findViewById(R.id.cuti);
        cuti.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getContext().getApplicationContext(), DaftarIzinActivity.class);
                tipe="cuti";
                intent.putExtra("tipe", tipe);
                intent.putExtra("id_pegawai", id_pegawai);
                intent.putExtra("id_status_pegawai", id_status_pegawai);
                startActivity(intent);
            }
        });

        bencana=(LinearLayout) v.findViewById(R.id.bencana);
        bencana.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getContext().getApplicationContext(), DaftarIzinActivity.class);
                tipe="bencana";
                intent.putExtra("tipe", tipe);
                intent.putExtra("id_pegawai", id_pegawai);
                intent.putExtra("id_status_pegawai", id_status_pegawai);
                startActivity(intent);
            }
        });

        ganti=(LinearLayout) v.findViewById(R.id.ganti);
        ganti.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getContext().getApplicationContext(), DaftarIzinActivity.class);
                tipe="ganti";
                intent.putExtra("tipe", tipe);
                intent.putExtra("id_pegawai", id_pegawai);
                intent.putExtra("id_status_pegawai", id_status_pegawai);
                startActivity(intent);
            }
        });

        apel=(LinearLayout) v.findViewById(R.id.apel);
        apel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getContext().getApplicationContext(), DaftarIzinActivity.class);
                tipe="apel";
                intent.putExtra("tipe", tipe);
                intent.putExtra("id_pegawai", id_pegawai);
                intent.putExtra("id_status_pegawai", id_status_pegawai);
                startActivity(intent);
            }
        });
        mApiService = UtilsAPI.getApiService();

        beranda();

        return v;
    }

    private void beranda(){
        String key="7d063bfb561bda7078d649dfeafe9aee";
        mApiService.ringkasan(key, id_pegawai)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            /*loading.dismiss();*/
                            JSONObject jsonRESULTS = null;
                            try {
                                jsonRESULTS = new JSONObject(response.body().string());
                                /*if (jsonRESULTS.getString("status").equals("false")) {
                                    Toast.makeText(getContext().getApplicationContext(), "Input yang anda masukan salah, silahkan coba kembali.", Toast.LENGTH_LONG).show();
                                }else {*/
                                    /*String id_pegawai = jsonRESULTS.getString("id_pegawai");
                                    String id_status_pegawai = jsonRESULTS.getString("id_status_pegawai");*/

                                    /*Intent intent = new Intent(getContext().getApplicationContext(), MainActivity.class);
                                    intent.putExtra("id_pegawai", id_pegawai);
                                    intent.putExtra("id_status_pegawai", id_status_pegawai);
                                    startActivity(intent);*/
                                /*}*/

                                jumlah_minggu.setText(jsonRESULTS.getString("jumlah_minggu"));
                                jumlah_bulan.setText(jsonRESULTS.getString("jumlah_bulan"));
                                jumlah_tahun.setText(jsonRESULTS.getString("jumlah_tahun"));
                                jumlah_sisa_cuti.setText(jsonRESULTS.getString("jumlah_sisa_cuti"));
                                jumlah_jaldin.setText(jsonRESULTS.getString("jumlah_jaldin"));
                                jumlah_tugas_belajar.setText(jsonRESULTS.getString("jumlah_tugas_belajar"));
                                jumlah_sakit.setText(jsonRESULTS.getString("jumlah_sakit"));
                                jumlah_cuti.setText(jsonRESULTS.getString("jumlah_cuti"));
                                jumlah_bencana.setText(jsonRESULTS.getString("jumlah_bencana"));
                                jumlah_ganti.setText(jsonRESULTS.getString("jumlah_ganti"));
                                jumlah_apel.setText(jsonRESULTS.getString("jumlah_apel"));


                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        } else {
                            /*loading.dismiss();*/
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        /*Toast.makeText(getContext().getApplicationContext(), "Input yang anda masukan salah, silahkan coba kembali.", Toast.LENGTH_LONG).show();
                        loading.dismiss();*/
                    }
                });
    }

}

