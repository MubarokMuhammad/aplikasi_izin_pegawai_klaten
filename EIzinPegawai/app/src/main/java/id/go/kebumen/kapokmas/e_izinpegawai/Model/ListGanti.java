package id.go.kebumen.kapokmas.e_izinpegawai.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListGanti {


    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("tanggal")
    @Expose
    private String tanggal;

    @SerializedName("tipe")
    @Expose
    private String tipe;

    @SerializedName("file_bukti")
    @Expose
    private String file_bukti;

    @SerializedName("keterangan")
    @Expose
    private String keterangan;


    public ListGanti(String id, String tanggal, String tipe, String file_bukti, String keterangan) {
        this.id = id;
        this.tanggal = tanggal;
        this.tipe = tipe;
        this.file_bukti = file_bukti;
        this.keterangan = keterangan;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getFile_bukti() {
        return file_bukti;
    }

    public void setFile_bukti(String file_bukti) {
        this.file_bukti = file_bukti;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

}
