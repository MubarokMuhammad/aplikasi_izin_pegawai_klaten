package id.go.kebumen.kapokmas.e_izinpegawai;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.go.kebumen.kapokmas.e_izinpegawai.Adapter.AdapterJaldin;
import id.go.kebumen.kapokmas.e_izinpegawai.Api.BaseApiService;
import id.go.kebumen.kapokmas.e_izinpegawai.Api.UtilsAPI;
import id.go.kebumen.kapokmas.e_izinpegawai.Model.ListJaldin;
import id.go.kebumen.kapokmas.e_izinpegawai.Model.ResponseJaldin;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DaftarIzinActivity extends AppCompatActivity {
    private RecyclerView mRecycler;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mManager;
    private List<ListJaldin> mItems = new ArrayList<>();
    ProgressDialog pd;

    private List<ResponseJaldin> mCategoryDataList = new ArrayList<>();
    private List<ListJaldin> listCategory = new ArrayList<>();
    private RecyclerView rc_list_rating;

    private BaseApiService mApiService;
    private AdapterJaldin CategoryAdapter;

    private ProgressBar pb_load;
    private ShimmerRecyclerView shimmer_recycler_view;

    String id, key, id_pegawai, tipe, tahun, id_status_pegawai;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_izin);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mApiService = UtilsAPI.getApiService();

        rc_list_rating = (RecyclerView) findViewById(R.id.rc_list_category);
        pb_load = (ProgressBar) findViewById(R.id.pb_load);
        shimmer_recycler_view = (ShimmerRecyclerView) findViewById(R.id.shimmer_recycler_view);

        Intent i = getIntent();
        tipe=i.getStringExtra("tipe");
        id_pegawai=i.getStringExtra("id_pegawai");
        id_status_pegawai=i.getStringExtra("id_status_pegawai");

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DaftarIzinActivity.this, TambahIzinActivity.class);
                intent.putExtra("id_pegawai", id_pegawai);
                intent.putExtra("id_status_pegawai", id_status_pegawai);
                startActivity(intent);
            }
        });

        /*Toast.makeText(DaftarIzinActivity.this, "Tipe= "+tipe, Toast.LENGTH_LONG).show();*/
        // horizontal
        LinearLayoutManager layoutManagerCategory
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        // ini vertical
        RecyclerView.LayoutManager mLayoutManager =
                new LinearLayoutManager(this.getApplicationContext());

        CategoryAdapter = new AdapterJaldin(listCategory, this);
        rc_list_rating.setLayoutManager(mLayoutManager);
        rc_list_rating.setItemAnimator(new DefaultItemAnimator());
        rc_list_rating.setAdapter(CategoryAdapter);
    }


    private void dataAttachmentCategory() {

        rc_list_rating.setVisibility(View.GONE);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        tahun = sdf.format(new Date());

        key="7d063bfb561bda7078d649dfeafe9aee";
        /*id_pegawai="5ccbb39599fe3";*/

        mCategoryDataList.clear();
        listCategory.clear();
        mApiService.getJaldin(
                tipe,
                key,
                id_pegawai, tahun)
                .enqueue(new Callback<List<ListJaldin>>() {
                    @Override
                    public void onResponse(Call<List<ListJaldin>> call, Response<List<ListJaldin>> response) {

                        pb_load.setVisibility(View.GONE);
                        shimmer_recycler_view.hideShimmerAdapter();
                        List<ListJaldin> listCategories = response.body();
                        if (listCategories != null&& listCategories.size()>0) {
                            for (int i = 0; i < listCategories.size(); i++) {
                                ListJaldin modelSeatGroup = new ListJaldin(
                                        response.body().get(i).getId(),
                                        response.body().get(i).getTanggal(),
                                        response.body().get(i).getTipe(),
                                        response.body().get(i).getFile_bukti(),
                                        response.body().get(i).getKeterangan());
                                listCategory.add(modelSeatGroup);
                            }

                            CategoryAdapter = new AdapterJaldin(listCategory, DaftarIzinActivity.this);
                            rc_list_rating.setAdapter(CategoryAdapter);

                            if (listCategory.isEmpty()) {
                                rc_list_rating.setVisibility(View.GONE);

                                Toast.makeText(DaftarIzinActivity.this, "Data tidak ditemukan", Toast.LENGTH_SHORT).show();
                            } else {
                                rc_list_rating.setVisibility(View.VISIBLE);
                            }
                            //get values
                        }else{
                            //show alert not get value
                        }

                    }


                    @Override
                    public void onFailure(Call<List<ListJaldin>> call, Throwable t) {
                        pb_load.setVisibility(View.GONE);
                        shimmer_recycler_view.hideShimmerAdapter();
                        Toast.makeText(DaftarIzinActivity.this, "Silahkan cek kembali koneksi anda.", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();

        pb_load.setVisibility(View.GONE);
        shimmer_recycler_view.showShimmerAdapter();
        dataAttachmentCategory();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    this);
            alertDialogBuilder
                    .setTitle("Tentang")
                    .setMessage("sipp.klatenkab.go.id\nVersion 1.0")
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
            return true;
        }else if (id==android.R.id.home){
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
