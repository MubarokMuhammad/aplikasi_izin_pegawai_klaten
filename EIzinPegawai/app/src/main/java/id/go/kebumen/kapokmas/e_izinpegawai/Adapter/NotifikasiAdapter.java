package id.go.kebumen.kapokmas.e_izinpegawai.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ipaulpro.afilechooser.utils.FileUtils;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import id.go.kebumen.kapokmas.e_izinpegawai.MainActivity;
import id.go.kebumen.kapokmas.e_izinpegawai.Model.ListApel;
import id.go.kebumen.kapokmas.e_izinpegawai.Model.ListJaldin;
import id.go.kebumen.kapokmas.e_izinpegawai.R;
import id.go.kebumen.kapokmas.e_izinpegawai.TambahIzinActivity;

public class NotifikasiAdapter extends RecyclerView.Adapter<NotifikasiAdapter.MyViewHolder> {

    private List<ListApel> moviesList;
    private Activity activity;
            Context context;
            String tgl, isi, judul;
    private Object Activity;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView judul, isi, waktu;
        public LinearLayout ll_layout_utama;


        public MyViewHolder(View view) {
            super(view);
            judul = view.findViewById(R.id.judul);
            isi = view.findViewById(R.id.isi);
            waktu=view.findViewById(R.id.waktu);
            ll_layout_utama = view.findViewById(R.id.ll_layout_utama);
        }
    }

        public NotifikasiAdapter(List<ListApel> moviesList, Activity activity) {
            this.moviesList = moviesList;
            this.activity = activity;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.listview_notif, parent, false);


            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            final ListApel movie = moviesList.get(position);


            holder.judul.setText(movie.getArticle());

            String ket= String.valueOf(Html.fromHtml(movie.getContent()));
            judul=movie.getArticle();
            isi= String.valueOf(Html.fromHtml(movie.getContent()));

            Locale idl = new Locale("in", "ID");
            String pattern = "EEEE, dd MMM yyyy";

            SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat myFormat = new SimpleDateFormat(pattern, idl);

            try {

                String reformattedStr = myFormat.format(fromUser.parse(movie.getPost_date()));
                holder.waktu.setText(reformattedStr+" \t"+movie.getPost_time());
                tgl=reformattedStr+" \t"+movie.getPost_time();

            } catch (ParseException e) {
                e.printStackTrace();
            }

            holder.ll_layout_utama.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(view.getRootView().getContext());
                    alertDialogBuilder.setTitle("Detail Pengumuman");
                    alertDialogBuilder.setMessage(
                                    "Judul:\n\t\t\t\t\t\t\t"+judul+
                                    "\nTanggal:\n\t\t\t\t\t\t\t"+tgl+
                                            "\nIsi:\n\t\t\t\t\t\t\t"+isi);
                    alertDialogBuilder.setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {

                                }
                            });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
            });

        }

        @Override
        public int getItemCount() {
            return moviesList.size();
        }

}

