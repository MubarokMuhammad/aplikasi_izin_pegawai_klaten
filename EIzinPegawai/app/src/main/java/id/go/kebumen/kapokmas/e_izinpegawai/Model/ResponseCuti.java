package id.go.kebumen.kapokmas.e_izinpegawai.Model;

import java.util.List;

public class ResponseCuti {
    private List<ListCuti> listCutis = null;

    public ResponseCuti(List<ListCuti> listCutis) {
        this.listCutis = listCutis;
    }

    public List<ListCuti> getListCutis() {
        return listCutis;
    }

    public void setListCutis(List<ListCuti> listCutis) {
        this.listCutis = listCutis;
    }
}
