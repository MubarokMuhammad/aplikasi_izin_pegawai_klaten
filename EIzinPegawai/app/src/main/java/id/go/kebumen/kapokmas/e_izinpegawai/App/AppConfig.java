package id.go.kebumen.kapokmas.e_izinpegawai.App;

public class AppConfig {

    // URL LIST ROOM
    public static String URL_LIST_ROOM = "http://rsuddrsoedirman.kebumenkab.go.id/api/list_all_data/";

    // URL DETAIL ROOM
    public static String URL_DETAIL_ROOM = "http://rsuddrsoedirman.kebumenkab.go.id/api/detail_data/";

    // URL CARI RS
    public static String URL_Dokter_ROOM = "http://rsuddrsoedirman.kebumenkab.go.id/api/get_jadwal_dokter/";
    public static String URL_DETAIL_DARAH = "http://spgdt.kebumenkab.go.id/api/bloods";
    public static String URL_Search_DARAH = "http://spgdt.kebumenkab.go.id/api/bloods";

}